# Champions League

This is a PHP (Laravel) project developed to simulate a Champions League.
## Installation


```bash
git clone https://bitbucket.org/devturgay/diceus-league.git
```

## Installation (2 Versions)

Update .env file

### 1. Via artisan on local

```bash
# install packages
composer install

# Create/Migrate Db
php artisan migrate

# Generate project key
php artisan key:generate

# Start the server
php artisan serve
```
### 2. Via docker

```bash
# Run docker-compose.yaml
docker-compose up
```

## Usage
Just browse the root directory of server (localhost:8000/)

Please make sure to update tests as appropriate.

## Roadmap

- [x] Add Migration Class
- [x] Add Seeding Class
- [x] Add Unit/Feature Test
- [x] LeaguesController
- [x] TeamsController
- [x] Implement Berger algorithm for fixtures
- [x] Implement game simulation algorithm
- [x] League predictions on current week

<p align="right">(<a href="#top">back to top</a>)</p>
