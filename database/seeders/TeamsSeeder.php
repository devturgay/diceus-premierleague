<?php

namespace Database\Seeders;

use App\Models\Teams;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberOfTeams = random_int(2,10)*2;
        $teams = [];
        for($i = 0; $i < $numberOfTeams; $i++){
            $randomHomeMin = random_int(65, 99);
            $randomAwayMin = random_int(65, 99);
            $teams[] = [
                'name' => Str::random(10),
                'home_power_min'=> $randomHomeMin,
                'home_power_max'=> random_int($randomHomeMin, 99),
                'away_power_min'=> $randomAwayMin,
                'away_power_max'=> random_int($randomAwayMin, 99),
            ];
        }
        Teams::insert($teams);
    }
}
