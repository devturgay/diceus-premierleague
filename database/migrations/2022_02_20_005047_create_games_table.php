<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->foreignId('home_team')->constrained('teams');
            $table->foreignId('away_team')->constrained('teams');
            $table->foreignId('league')->constrained('leagues');
            $table->integer('week_no');
            $table->integer('status')->default(0)->comment('0: upcoming; 1: played');
            $table->integer('winner')->default(0)->comment('0: unplayed; 1: home; 2: away; 3: draw');
            $table->integer('home_team_score')->default(0);
            $table->integer('away_team_score')->default(0);
            $table->integer('home_team_point')->default(0);
            $table->integer('away_team_point')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
