<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use stdClass;

class GamesTest extends TestCase
{
    /**
     * @test
     * @return void
     */
    public function a_game_can_be_played()
    {
        // Fake game data
        $game = new stdClass();
        $game->home_team_data = [
            "id" => 1,
            "name" => "Team 1",
            "status" => 1,
            "home_power_min" => 67,
            "home_power_max" => 94,
            "away_power_min" => 69,
            "away_power_max" => 78,
        ];
        $game->away_team_data = [
            "id" => 2,
            "name" => "Team 2",
            "status" => 1,
            "home_power_min" => 67,
            "home_power_max" => 68,
            "away_power_min" => 96,
            "away_power_max" => 98,
        ];

        // Simulation constants
        $simConstants = [
            'minDiff2Win'   => 3, // Minimal power difference to win a game
            'wonPoint'      => 3,
            'lostPoint'     => 0,
            'drawPoint'     => 1,
            'drawScoreMax'  => 3
        ];

        // Play a game
        $isArray = is_array(simulateTheGame($game, $simConstants));

        $this->assertTrue($isArray);
    }

    /**
     * @test
     * @return void
     */
    public function a_group_of_teams_can_be_fixtured()
    {
        // Fake teams
        $teams = [1,2,3,4];

        // Create first half games
        $firstHalfGames = generateFirstHalfGames($teams);
        $this->assertTrue(is_array($firstHalfGames));

        // Create second half games
        $isArray = is_array(generateSecondHalfGames($firstHalfGames));
        $this->assertTrue($isArray);
    }
}
