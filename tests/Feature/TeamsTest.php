<?php

namespace Tests\Feature;

use App\Models\Teams;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Tests\TestCase;

class TeamsTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @test
     */
    public function a_team_can_be_created()
    {
        // To refresh auto_incrementing
        DB::table("leagues_teams")->delete();
        DB::table("games")->delete();
        DB::table("teams")->delete();
        DB::select("ALTER TABLE `teams` AUTO_INCREMENT = 1");

        $nextId     = 1;
        $randomHomeMin = random_int(5, 89);
        $randomAwayMin = random_int(5, 89);
        $team       = [
            'name'=>Str::random(10),
            'home_power_min'=> $randomHomeMin,
            'home_power_max'=> random_int($randomHomeMin, 99),
            'away_power_min'=> $randomAwayMin,
            'away_power_max'=> random_int($randomAwayMin, 99),
        ];
        $response   = $this->post('/teams',$team);
        $createdId  = Teams::max('id');

        $response->assertStatus(200);
        $this->assertEquals($nextId, $createdId);
        return $createdId;
    }

    /**
     * @test
     * @depends a_team_can_be_created
     */
    public function a_team_can_be_edited($teamId)
    {
        $newName = Str::random(10);
        $randomHomeMin = random_int(5, 89);
        $randomAwayMin = random_int(5, 89);
        $team = [
            'name'=>$newName,
            'home_power_min'=> $randomHomeMin,
            'home_power_max'=> random_int($randomHomeMin, 99),
            'away_power_min'=> $randomAwayMin,
            'away_power_max'=> random_int($randomAwayMin, 99),
        ];

        $response = $this->put("/teams/$teamId",$team);
        $response->assertStatus(200);

        $team = Teams::find($teamId);
        $this->assertEquals($newName, $team->name);
    }

    /**
     * @test
     * @depends a_team_can_be_created
     */
    public function a_team_can_be_deleted($teamId)
    {
        $response = $this->delete("/teams/$teamId");
        $response->assertStatus(200);

        $team = Teams::find($teamId);
        $this->assertEquals(null, $team);
    }
}
