<?php

namespace Tests\Feature;

use App\Models\Leagues;
use App\Models\Teams;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Support\Str;

class LeaguesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function a_league_can_be_created_based_on_teams()
    {
        DB::select("ALTER TABLE `leagues` AUTO_INCREMENT = 1"); // To refresh auto_incrementing
        Artisan::call('db:seed'); // Generate teams between number of 4-20

        $nextId = 1;
        $league = [
            'name'  => "League - $nextId",
            'teams' => array_column(Teams::all()->toArray(), 'id')
        ];
        $response = $this->post('/leagues',$league);
        $response->assertStatus(200);

        $createdId = Leagues::max('id');
        $this->assertEquals($nextId, $createdId);
        return $createdId;
    }

    /**
     * @test
     * @depends a_league_can_be_created_based_on_teams
     */
    public function a_league_can_be_edited_for_its_name($leagueId)
    {
        $newName    = Str::random(10);
        $league       = ['name'=>$newName];

        $response = $this->put("/leagues/$leagueId", $league);
        $response->assertStatus(200);

        $league = Leagues::find($leagueId);
        $this->assertEquals($newName, $league->name);
    }

    /**
     * @test
     * @depends a_league_can_be_created_based_on_teams
     */
    public function a_league_can_be_deleted($leagueId)
    {
        $response = $this->delete("/leagues/$leagueId");
        $response->assertStatus(200);

        $league = Leagues::find($leagueId);
        $this->assertEquals(null, $league);
    }

    /**
     * @test
     * @depends a_league_can_be_created_based_on_teams
     */
    public function a_league_can_be_started($leagueId)
    {
        $response = $this->get("/leagues/start/$leagueId");
        $response->assertStatus(200);

        return $leagueId;
    }

    /**
     * @test
     * @depends a_league_can_be_started
     */
    public function a_week_can_be_played($leagueId)
    {
        $response = $this->get("/leagues/playweek/$leagueId");
        $response->assertStatus(200);
    }

    /**
     * @test
     * @depends a_league_can_be_started
     */
    public function all_weeks_can_be_played_at_once($leagueId)
    {
        $response = $this->get("/leagues/playallweeks/$leagueId");
        $response->assertStatus(200);
    }
}
