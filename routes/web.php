<?php

use App\Http\Controllers\LeaguesController;
use App\Http\Controllers\TeamsController;
use App\Models\Leagues;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('leagues.index'));
});

Route::resources([
    'teams' => TeamsController::class,
    'leagues' => LeaguesController::class,
]);

Route::put('/teams/status/{team}', [TeamsController::class, 'updateStatus'])->name('teams.status');

Route::get('/leagues/start/{id}', [LeaguesController::class, 'startLeague'])->name('leagues.start');
Route::get('/leagues/playweek/{id}', [LeaguesController::class, 'playWeek'])->name('leagues.playweek');
Route::get('/leagues/playallweeks/{id}', [LeaguesController::class, 'playAllWeeks'])->name('leagues.playAllWeeks');
