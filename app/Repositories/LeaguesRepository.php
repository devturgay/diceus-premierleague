<?php

namespace App\Repositories;

use App\Models\Leagues;
use Illuminate\Http\Request;

class LeaguesRepository
{
    /**
     * @var $leagues (Model)
     */
    protected $leagues;

    /**
     * Leagues Repository constructor
     *
     * @param Leagues $leagues
     */
    function __construct(Leagues $leagues)
    {
        $this->leagues = $leagues;
    }

    /**
     * Fetches all leagues and orders them first by status (DESC), then id (DESC)
     * Parses to array
     *
     * @return array
     */
    public function fetchAllLeaguesOrderByStatusAndId()
    {
        return Leagues::orderBy('status', 'ASC')->orderBy('id', 'DESC')->get()->toArray();
    }

    /**
     * Create a league
     *
     * @return $league
     */
    public function createLeague(string $name)
    {
        $league = new Leagues;
        $league->name = $name;
        $league->save();
        return $league;
    }

    /**
     * Attach all teams to the league
     */
    public function attachTeamsToLeague(array $teams, $league)
    {
        $league->teams()->attach($teams);
    }

    /**
     * Start the league
     */
    public function startLeague(int $leagueId)
    {
        $league = Leagues::find($leagueId);
        $league->status = 1;
        $league->save();
    }
}
