<?php

namespace App\Repositories;

use App\Models\Teams;
use Illuminate\Http\Request;

class TeamsRepository
{
    /**
     * @var Teams
     */
    protected $teams;

    /**
     * TeamsRepository constructor
     *
     * @param Teams $teams
     */
    function __construct(Teams $teams)
    {
        $this->teams = $teams;
    }

    /**
     * Store a new team
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        $team = $this->teams;
        $team->name = $request->name;
        $team->home_power_min = $request->home_power_min;
        $team->home_power_max = $request->home_power_max;
        $team->away_power_min = $request->away_power_min;
        $team->away_power_max = $request->away_power_max;
        return $team->save();
    }

    /**
     * Update a team
     *
     * @param Request $request
     * @param int $id
     */
    public function updateTeam(Request $request, int $id)
    {
        $updateData = [
            'name' => $request->name,
            'home_power_min' => $request->home_power_min,
            'home_power_max' => $request->home_power_max,
            'away_power_min' => $request->away_power_min,
            'away_power_max' => $request->away_power_max,
        ];
        return $this->teams::where('id', $id)
            ->update($updateData);
    }

    /**
     * Update a team
     *
     * @param Request $request
     * @param int $id
     */
    public function updateTeamStatus(Request $request, int $team)
    {
        $team = $this->teams::find($team);
        $team->status = $request->status;
        return $team->save();
    }

    /**
     * Fetch team's data
     *
     * @param int $teamId
     * @return array
     */
    public function teamDataById(int $teamId)
    {
        return Teams::where('id', $teamId)->get()->toArray()[0];
    }
}
