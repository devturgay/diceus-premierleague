<?php

namespace App\Repositories;

use App\Models\Games;
use Illuminate\Support\Facades\DB;

class GamesRepository
{
    /**
     * @var Games
     */
    protected $game;

    /**
     * GamesRepository constructor
     *
     * @param Games $game
     */
    function __construct(Games $game)
    {
        $this->game = $game;
    }

    /**
     * Insert games to db
     *
     * @param array $allGames
     * @param int $gamesInEachWeek
     * @param int $leagueId
     *
     * @return void
     */
    public function createGamesMultiple(array $allGames, int $gamesInEachWeek, int $league)
    {
        $insertingData = [];
        $weekNo = 1;
        $gameCounter = 0;
        foreach ($allGames as $game) {
            if ($gameCounter == $gamesInEachWeek) {
                $gameCounter = 0;
                $weekNo++;
            }
            $insertingData[] = [
                'home_team' => $game[0],
                'away_team' => $game[1],
                'league'    => $league,
                'week_no'   => $weekNo
            ];
            $gameCounter++;
        }

        Games::insert($insertingData);
    }

    /**
     * Fetch all games of league with the names of teams
     *
     * @param int $leagueId
     */
    public function leagueGamesAll(int $leagueId)
    {
        return DB::table('games as g')
            ->selectRaw("*,
            (SELECT name from teams WHERE teams.id = g.home_team) as home_team_name,
            (SELECT name from teams WHERE teams.id = g.away_team) as away_team_name")
            ->where("league", $leagueId)->get();
    }

    /**
     * Fetch games of the upcoming week by league Id
     *
     * @param int $leagueId
     * @return array
     */
    public function upcomingWeekGames(int $leagueId): array
    {
        return DB::select(
            "SELECT * FROM games WHERE week_no = " .
                "(SELECT MIN(week_no) FROM games WHERE status=0 AND league=$leagueId)"
        );
    }
    /**
     * Fetch games of the ALL upcoming weeks by league Id
     *
     * @param int $leagueId
     * @return array
     */
    public static function upcomingWeekGamesAll(int $leagueId): array
    {
        return DB::select(
            "SELECT * FROM games WHERE week_no IN " .
                "(SELECT week_no FROM games WHERE status=0 AND league=$leagueId)"
        );
    }

    /**
     * Fetch list of unplayed games of league
     *
     * @param int $laegueId
     * @return array
     */
    public function unplayedGames(int $leagueId)
    {
        return Games::where('status', 0)->where('league', $leagueId)->get()->toArray();
    }

    /**
     * Update Game with the simulated results
     *
     * @param int $gameId
     * @param array $result
     */
    public function updateGameResults(int $gameId, array $result)
    {
        $playedGame = Games::find($gameId);
        $playedGame->status = 1;
        $playedGame->home_team_score = $result['home']['score'];
        $playedGame->away_team_score = $result['away']['score'];
        $playedGame->home_team_point = $result['home']['point'];
        $playedGame->away_team_point = $result['away']['point'];
        $playedGame->winner = $result['winner'];
        $playedGame->save();
    }
}
