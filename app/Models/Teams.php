<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'power'];

    public function leagues()
    {
        return $this->belongsToMany(Leagues::class, 'leagues_teams', 'team', 'league');
    }
}
