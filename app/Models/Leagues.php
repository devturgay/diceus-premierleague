<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Leagues extends Model
{
    use HasFactory;

    public function teams()
    {
        return $this->belongsToMany(Teams::class, 'leagues_teams', 'league', 'team');
    }

    public static function leagueTable($leagueId)
    {
        return DB::table('teams as t')
            ->select('t.*')
            ->selectRaw("(
                SELECT CONCAT(
                    SUM(home_team_score),
                    '-',
                    SUM(away_team_score),
                    '-',
                    SUM(home_team_point),
                    '-',
                    (SELECT COUNT(id) as c_won FROM games WHERE winner = 1 and games.home_team = t.id AND games.league = $leagueId),
                    '-',
                    (SELECT COUNT(id) as c_lost FROM games WHERE winner = 2 and games.home_team = t.id AND games.league = $leagueId),
                    '-',
                    (SELECT COUNT(id) as c_draw FROM games WHERE winner = 3 and games.home_team = t.id AND games.league = $leagueId)
                ) as sm
                 FROM games WHERE games.home_team = t.id AND games.league = $leagueId
            ) as in_home")
            ->selectRaw("(
                SELECT CONCAT(
                    SUM(away_team_score),
                    '-',
                    SUM(home_team_score),
                    '-',
                    SUM(away_team_point),
                    '-',
                    (SELECT COUNT(id) as c_won FROM games WHERE winner = 2 and games.away_team = t.id AND games.league = $leagueId),
                    '-',
                    (SELECT COUNT(id) as c_lost FROM games WHERE winner = 1 and games.away_team = t.id AND games.league = $leagueId),
                    '-',
                    (SELECT COUNT(id) as c_draw FROM games WHERE winner = 3 and games.away_team = t.id AND games.league = $leagueId)
                ) as sm
                 FROM games WHERE games.away_team = t.id AND games.league = $leagueId
            ) as in_away")
            ->join('leagues_teams', 'leagues_teams.team', '=', 't.id')
            ->where('leagues_teams.league', $leagueId)
            ->get();
    }
}
