<?php

namespace App\Services;

use App\Models\Games;
use App\Models\Teams;
use App\Repositories\GamesRepository;
use App\Repositories\TeamsRepository;
use App\Services\Leagues\LeaguesProcessService;
use Illuminate\Support\Facades\Config;

class GamesService
{
    /**
     * @var $gamesRepository
     */
    protected $gamesRepository;

    /**
     * @var $gamesRepository
     */
    protected $teamsRepository;

    /**
     * Games Service constructor
     */
    function __construct()
    {
        $this->gamesRepository = new GamesRepository(new Games);
        $this->teamsRepository = new TeamsRepository(new Teams);
    }

    /**
     * Play a week of ongoing league
     *
     * @param int $leagueId
     * @param bool $all = false
     * @return void
     */
    public function playWeek(int $leagueId, bool $all = false)
    {
        // Fetch the upcoming week and its games
        $games = $all ?
            $this->gamesRepository->upcomingWeekGamesAll($leagueId)
            :
            $this->gamesRepository->upcomingWeekGames($leagueId);

        // Play (simulate) the games
        $this->playGames($games);

        // If this was the last week, finish league
        if (empty($this->gamesRepository->unplayedGames($leagueId))) {
            (new LeaguesProcessService)->finishLeague($leagueId);
        }
    }

    /**
     * Simulates a set of games and return results
     *
     * @param array $games
     */
    public function playGames(array $games)
    {
        foreach ($games as $game) {
            // Fetch teams data
            $game->home_team_data = $this->teamsRepository->teamDataById($game->home_team);
            $game->away_team_data = $this->teamsRepository->teamDataById($game->away_team);

            // Simulate the game
            $result = simulateTheGame($game, Config::get('app.simulationConstants'));

            // Update db with results
            $this->gamesRepository->updateGameResults($game->id, $result);
        }
    }
}
