<?php

/**
 * The service handles all processes to create a league and fetch them
 *
 * Using leagues and games repositories
 */

namespace App\Services\Leagues;

use App\Models\Games;
use App\Models\Leagues;
use App\Repositories\GamesRepository;
use App\Repositories\LeaguesRepository;
use Illuminate\Http\Request;

class LeaguesCreationService
{

    /**
     * @var $leaguesRepository
     */
    protected $leaguesRepository;

    /**
     * @var $gamesRepository
     */
    protected $gamesRepository;

    /**
     * Leagues Repository constructor
     */
    function __construct()
    {
        $this->leaguesRepository = new LeaguesRepository(new Leagues);
        $this->gamesRepository = new GamesRepository(new Games);
    }

    /**
     * Fetches leagues filters them as pendings, actives and finisheds
     *
     * @return array
     */
    public function fetchAllLeaguesFiltered()
    {
        $leagues = $this->leaguesRepository->fetchAllLeaguesOrderByStatusAndId();
        $pendingLeagues = array_filter($leagues, function ($v) {
            return $v['status'] == 0;
        });
        $activeLeagues = array_filter($leagues, function ($v) {
            return $v['status'] == 1;
        });
        $finishedLeagues = array_filter($leagues, function ($v) {
            return $v['status'] == 2;
        });
        return compact('pendingLeagues', 'activeLeagues', 'finishedLeagues');
    }

    /**
     * Stores a league and attaches the teams to it
     * Creates fixtures and store the games
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required|string|min:2|max:254',
            'teams' => 'required|array|min:4|max:20'
        ]);

        // Create League
        $league = $this->leaguesRepository->createLeague($request->name);
        // Attach teams
        $this->leaguesRepository->attachTeamsToLeague($request->teams, $league);

        // Create weeks
        $this->tossUpTeamsGamesWeeks($request->teams, $league->id);

        return true;
    }

    /**
     * Creates leagues_teams relation
     * @param array $teams
     * @param int $leagueId
     *
     * @return void
     */
    public function tossUpTeamsGamesWeeks(array $teams, int $leagueId)
    {
        // First half fixtures
        $firstHalfGames = generateFirstHalfGames($teams);

        $gamesInEachWeek = count($teams) / 2;

        // Second half fixtures (mirror [first half fixtures])
        $secondHalfGames = generateSecondHalfGames($firstHalfGames);

        $allGames = array_merge($firstHalfGames, $secondHalfGames);

        // Insert games to db
        $this->gamesRepository->createGamesMultiple($allGames, $gamesInEachWeek, $leagueId);
    }

    /**
     * Fetch games of the league filtered on status
     *
     * @param int $leagueId
     * @return array
     */
    public function fetchGamesOfLeaguesFiltered(int $leagueId)
    {
        $allGames = json_decode(json_encode( // Deep parse to array
            $this->gamesRepository->leagueGamesAll($leagueId)
        ), true);

        return [
            'playedGames' => array_reverse(array_filter($allGames, function ($v) {
                return $v['status'] == 1;
            })),
            'upcomingGames' => array_filter($allGames, function ($v) {
                return $v['status'] == 0;
            }),
        ];
    }
}
