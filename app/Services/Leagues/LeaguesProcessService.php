<?php

/**
 * The service handles all processes to start and play a league
 */

namespace App\Services\Leagues;

use App\Models\Games;
use App\Models\Leagues;
use App\Repositories\GamesRepository;
use App\Repositories\LeaguesRepository;

class LeaguesProcessService
{
    /**
     * @var $leaguesRepository
     */
    protected $leaguesRepository;

    /**
     * @var $gamesRepository
     */
    protected $gamesRepository;

    /**
     * Leagues Repository constructor
     */
    function __construct()
    {
        $this->leaguesRepository = new LeaguesRepository(new Leagues);
        $this->gamesRepository = new GamesRepository(new Games);
    }

    /**
     * Start the league
     *
     * @param int $leagueId
     */
    public function startLeague(int $leagueId)
    {
        // Update league status to ongoing (1)
        $this->leaguesRepository->startLeague($leagueId);
    }

    /**
     * Finishes the league (updates status to 2)
     *
     * @param int $leagueId
     * @return void
     */
    public function finishLeague(int $leagueId)
    {
        $league = Leagues::find($leagueId);
        $league->status = 2;
        $league->save();
    }
}
