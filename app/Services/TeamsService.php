<?php

namespace App\Services;

use App\Models\Teams;
use App\Repositories\TeamsRepository;
use Illuminate\Http\Request;

class TeamsService
{
    /**
     * Teams Repository
     * @var $teamsRepository
     */
    protected $teamsRepository;

    /**
     * Teams Model
     * @var $teamsModel
     */
    protected $teamsModel;

    /**
     * Teams Service constructor
     */
    function __construct()
    {
        $this->teamsRepository = new TeamsRepository(new Teams);
    }

    /**
     * Validate team request to store
     * Call repo to store a team
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        // Validate
        $request->validate([
            'name'              => 'required|string|min:2|max:254',
            'home_power_min'    => 'required|min:1|max:3',
            'home_power_max'    => 'required|min:1|max:3',
            'away_power_min'    => 'required|min:1|max:3',
            'away_power_max'    => 'required|min:1|max:3',
        ]);

        return $this->teamsRepository->store($request);
    }

    /**
     * Validate request to update a team
     * Call repo to update a team
     *
     * @param Request $request
     * @param int $id
     */
    public function updateTeam(Request $request, int $id)
    {
        // Validate
        $request->validate([
            'name'              => 'required|string|min:2|max:254',
            'home_power_min'    => 'required|min:1|max:3',
            'home_power_max'    => 'required|min:1|max:3',
            'away_power_min'    => 'required|min:1|max:3',
            'away_power_max'    => 'required|min:1|max:3',
        ]);

        return $this->teamsRepository->updateTeam($request, $id);
    }

    /**
     * Validate team request to update the status
     * Call repo to update the status
     *
     * @param Request $request
     * @param int $id
     */
    public function updateTeamStatus(Request $request, int $team)
    {
        // Validate
        $request->validate([
            'status' => 'required|min:1'
        ]);

        return $this->teamsRepository->updateTeamStatus($request, $team);
    }
}
