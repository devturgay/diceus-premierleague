<?php

/**
 * Wraps view function to load the layouts
 * @return view
 */
function view_layout(string $template, array $data = [])
{
    return view('default', ['template' => $template, 'data' => $data]);
}

/**
 * Generates the random matches for first half of league
 * Mathcing rule:
 * 1. Each team must play at least and only 1 game per week
 * @return array
 */
function generateFirstHalfGames(array $teams)
{
    // Apply Berger Algorithm
    return bergerFixture(bergerTeams($teams));
}

/**
 * Berger fixture Algorithm
 * @return array
 */
function bergerFixture($bergerTeams)
{
    // Start matching
    $matchedGames = [];
    $numOfTeams = count($bergerTeams);
    $matchIndex = $numOfTeams / 2;
    for ($i = 0; $i < $numOfTeams - 1; $i++) {
        // Create matches
        for ($j = 0; $j < $matchIndex; $j++) {
            $matchedGames[] = [$bergerTeams[$j], $bergerTeams[$j + $matchIndex]];
        }
        // After one row, push each element one step forward, except first
        // First Half
        for ($x = 1; $x < $matchIndex - 1; $x++) { // First iteration
            if ($x == 1) {
                $temp = $bergerTeams[$x + 1];
                $bergerTeams[$x + 1] = $bergerTeams[$x];
                continue;
            }
            $thisVal = $temp;
            $temp = $bergerTeams[$x + 1];
            $bergerTeams[$x + 1] = $thisVal;
        }
        // Second Half
        for ($x = $numOfTeams - 1; $x > $matchIndex; $x--) {
            if ($x == $numOfTeams - 1) { // First iteration
                $thisVal = $bergerTeams[$x];
                $bergerTeams[$x] = $temp ?? $bergerTeams[1]; // 4 teams situation
                $temp = $bergerTeams[$x - 1];
                $bergerTeams[$x - 1] = $thisVal;
                if ($numOfTeams == 4)
                    $bergerTeams[1] = $temp;
                continue;
            }
            $thisVal = $temp;
            $temp = $bergerTeams[$x - 1];
            $bergerTeams[$x - 1] = $thisVal;
            if ($x - 1 == $matchIndex) { // Last iteration
                $bergerTeams[1] = $temp;
            }
        }
    }
    return $matchedGames;
}

/**
 * Prepare the given teams array to be "bergered"
 * 1. The list should be divided by 2
 * 2. The second half of list should be reversed
 * @return array
 */
function bergerTeams($teams)
{
    // Divide array to 2,
    $bergerTeams = array_chunk($teams, count($teams) / 2);
    // Reverse second half
    $bergerTeams[1] = array_reverse($bergerTeams[1]);
    // Merge the arrays again
    $bergerTeams = array_merge($bergerTeams[0], $bergerTeams[1]);

    return $bergerTeams;
}

/**
 * Generates the second half of league based on first
 * @return void
 */
function generateSecondHalfGames($firstHalfGames)
{
    foreach ($firstHalfGames as &$value) {
        $value = array_reverse($value);
    }
    return $firstHalfGames;
}

/**
 * Simulates the game
 * Simulation rules:
 * 1. Find out the powers:
 *  1.1 Finding out a random power range ($homeTeamPower) between [home_min and home_max] for home_team
 *  1.2 Finding out a random power range ($awayTeamPower) between [away_min and away_max] for away_team
 * 2. Find out the winning team by comparing the powers ($powerDiff)
 *  2.1 If the power difference is not more than 3 it is draw
 * 3. Score goals:
 *  3.1 Score goals for winning team by powerDifference/3
 *  3.2 Score goals for loosing team by a random num between [0, (goalsOfWinninTeam - 1)]
 *  3.3 If it is a draw, Score goals for each time by a number between [0, 3]
 * @return object
 */
function simulateTheGame($game, array $simConsts)
{
    $HTD = $game->home_team_data;
    $ATD = $game->away_team_data;

    // Find out the powers
    $homeTeamPower = random_int($HTD['home_power_min'], $HTD['home_power_max']);
    $awayTeamPower = random_int($ATD['away_power_min'], $ATD['away_power_max']);
    $powerDiff = abs($homeTeamPower - $awayTeamPower);

    // Find out the winning team
    $winning = $powerDiff <= $simConsts['minDiff2Win'] ? false : ($homeTeamPower > $awayTeamPower ? 'home_team' : 'away_team');

    // Calculate the scored goals
    switch ($winning) {
        case 'home_team':
            $homeTeamResult = [
                'score' => intval($powerDiff / 3),
                'point' => $simConsts['wonPoint']
            ];
            $awayTeamResult = [
                'score' => random_int(0, $homeTeamResult['score'] - 1),
                'point' => $simConsts['lostPoint']
            ];
            $winner = 1;
            break;
        case 'away_team':
            $awayTeamResult = [
                'score' => intval($powerDiff / 3),
                'point' => $simConsts['wonPoint']
            ];
            $homeTeamResult = [
                'score' => random_int(0, $awayTeamResult['score'] - 1),
                'point' => $simConsts['lostPoint']
            ];
            $winner = 2;
            break;
        default: // Draw
            $homeTeamResult = $awayTeamResult = [
                'score' => random_int(0, $simConsts['drawScoreMax']),
                'point' => $simConsts['drawPoint']
            ];
            $winner = 3;
            break;
    }

    return [
        'home' => $homeTeamResult,
        'away' => $awayTeamResult,
        'winner' => $winner
    ];
}
