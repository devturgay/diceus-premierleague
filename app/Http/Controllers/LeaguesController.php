<?php

namespace App\Http\Controllers;

use App\Models\Games;
use App\Models\Leagues;
use App\Models\Teams;
use App\Services\GamesService;
use App\Services\Leagues\LeaguesCreationService;
use App\Services\Leagues\LeaguesProcessService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use PDO;
use Throwable;

class LeaguesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = (new LeaguesCreationService)->fetchAllLeaguesFiltered();
        return view_layout('leagues.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Teams::all()->toArray();
        return view_layout('leagues/create', ['teams' => $teams]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            (new LeaguesCreationService)->store($request);
            return response('League created', 200);
        } catch (Throwable $error) {
            return response("League creation failed: {$error->getMessage()}", 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $league = Leagues::find($id);

        $games = (new LeaguesCreationService)->fetchGamesOfLeaguesFiltered($id);

        $viewsOnStatus = [
            0 => "upcomingLeagueShow",
            1 => "ongoingLeagueShow",
            2 => "finishedLeagueShow"
        ];
        return view_layout(
            "leagues.show.{$viewsOnStatus[$league->status]}",
            [
                'league' => $league,
                'games' => $games,
                'table' => $this->leagueTable($id)
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Leagues::where('id', $id)
            ->update(['name' => $request->name]);

        return response('League updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        DB::table('leagues_teams')->where('league', $id)->delete();
        Games::where('league', $id)->delete();
        Leagues::where('id', $id)->delete();

        return response('League deleted', 200);
    }

    /**
     * Start a league and play first week
     *
     * @param int $leagueId
     * @return void
     */
    public function startLeague(int $leagueId)
    {
        // Start the league
        (new LeaguesProcessService)->startLeague($leagueId);

        // Play first week
        (new GamesService)->playWeek($leagueId);
    }

    /**
     * Play a week
     *
     * @param int $leagueId
     */
    public function playWeek($leagueId)
    {
        // Play a weeks
        (new GamesService)->playWeek($leagueId);
    }

    /**
     * Wraps playWeek method and runs allweeks at once
     * @return void
     */
    public function playAllWeeks($leagueId)
    {
        // Play all weeks
        (new GamesService)->playWeek($leagueId, true);
    }

    /**
     * Fetch the current league table
     * @return view
     */
    public function leagueTable($leagueId)
    {
        $table = json_decode(
            json_encode(
                Leagues::leagueTable($leagueId)
            ),
            true
        );
        usort($table, function ($a, $b) {
            $a_homeData = explode('-', $a['in_home']);
            $a_awayData = explode('-', $a['in_away']);
            $a_point = $a_homeData[2] + $a_awayData[2];

            $b_homeData = explode('-', $b['in_home']);
            $b_awayData = explode('-', $b['in_away']);
            $b_point = $b_homeData[2] + $b_awayData[2];

            return $b_point - $a_point;
        });
        return $table;
    }
}
