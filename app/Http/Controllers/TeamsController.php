<?php

namespace App\Http\Controllers;

use App\Models\Teams;
use App\Services\TeamsService;
use Illuminate\Http\Request;
use Throwable;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Teams::all()->toArray();
        return view_layout('teams.index', ['teams' => $teams]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view_layout('teams/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            (new Teamsservice)->store($request);
            return response('Team created', 200);
        } catch (Throwable $error) {
            return response("Team creation failed: {$error->getMessage()}", 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Teams::find($id);
        return view_layout('teams.edit', ['team' => $team]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        try {
            (new TeamsService)->updateTeam($request, $id);
            return response('Team updated', 200);
        } catch (Throwable $error) {
            return response("Team update failed: {$error->getMessage()}", 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Teams::where('id', $id)
            ->delete();

        return response('Team deleted', 200);
    }

    /**
     * Updates team status
     *
     * @return void
     */
    public function updateStatus(Request $request, int $team)
    {
        try {
            (new TeamsService)->updateTeamStatus($request, $team);
            return response('Team status updated', 200);
        } catch (Throwable $error) {
            return response("Team status update failed: {$error->getMessage()}", 500);
        }
    }
}
