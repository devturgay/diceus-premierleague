<div class="row mb-4">
    <div class="col-12">
        <h3 class="float-left">Existing Leagues</h3>
        <button class="btn btn-primary float-right" onclick="window.location.href='{{route('leagues.create')}}'"><i class="fa fa-plus"></i> Add league</button>
    </div>
</div>
<hr>

<style>
    .thead-success th{
        background:#28a745;color:white;
    }
    .thead-primary th{
        background:#007bff;color:white;
    }
</style>
<h3>Upcoming Leagues</h3>
<div class="row mb-2">
    <div class="col-12">
        <table class="table table-hover">
            <thead class="thead-primary">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Legaue Name</th>
                    {{-- <th scope="col">Status</th> --}}
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php ($counter = 1)
                @foreach ($pendingLeagues as $league)
                <tr>
                    <th scope="row">{{ $counter }}</th>
                    <td>{{ $league['name'] }}</td>
                    {{-- <td>
                        <input
                            data-url="{{ route('leagues.index', ['league'=>$league['id']]) }}"
                            class="statusCheck"
                            type="checkbox"
                            {{ $league['status'] == 1 ? 'checked' : '' }}
                        >
                        <i class="fa fa-spinner fa-spin" style="display:none;"></i>
                    </td> --}}
                    <td>
                        <button class="btn btn-danger deleteLeague" data-url="{{ route('leagues.destroy', ['league'=>$league['id']]) }}">
                            <i class="fa fa-trash"></i>
                        </button>
                        <button class="btn btn-primary startLeague" data-url="{{ route('leagues.start', ['id'=>$league['id']]) }}">
                            <i class="fa fa-power-off"></i>
                        </button>
                    </td>
                </tr>
                @php ($counter++)
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<hr>
<h3>Ongoing Leagues</h3>
<div class="row mb-2">
    <div class="col-12">
        <table class="table table-hover">
            <thead class="thead-success">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Legaue Name</th>
                    {{-- <th scope="col">Status</th> --}}
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php ($counter = 1)
                @foreach ($activeLeagues as $league)
                <tr>
                    <th scope="row">{{ $counter }}</th>
                    <td>{{ $league['name'] }}</td>
                    {{-- <td>
                        <input
                            data-url="{{ route('leagues.index', ['league'=>$league['id']]) }}"
                            class="statusCheck"
                            type="checkbox"
                            {{ $league['status'] == 1 ? 'checked' : '' }}
                        >
                        <i class="fa fa-spinner fa-spin" style="display:none;"></i>
                    </td> --}}
                    <td>
                        <button class="btn btn-primary seeLeague" data-url="{{ route('leagues.show', ['league'=>$league['id']]) }}">
                            <i class="fa fa-eye"></i>
                        </button>
                    </td>
                </tr>
                @php ($counter++)
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<hr>
<h3>Finished Leagues</h3>
<div class="row mb-2">
    <div class="col-12">
        <table class="table table-hover">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Legaue Name</th>
                    {{-- <th scope="col">Status</th> --}}
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php ($counter = 1)
                @foreach ($finishedLeagues as $league)
                <tr>
                    <th scope="row">{{ $counter }}</th>
                    <td>{{ $league['name'] }}</td>
                    {{-- <td>
                        <input
                            data-url="{{ route('leagues.index', ['league'=>$league['id']]) }}"
                            class="statusCheck"
                            type="checkbox"
                            {{ $league['status'] == 1 ? 'checked' : '' }}
                        >
                        <i class="fa fa-spinner fa-spin" style="display:none;"></i>
                    </td> --}}
                    <td>
                        <button class="btn btn-primary seeLeague" data-url="{{ route('leagues.destroy', ['league'=>$league['id']]) }}">
                            <i class="fa fa-eye"></i>
                        </button>
                    </td>
                </tr>
                @php ($counter++)
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
    $('body').on('click', '.deleteLeague', function(){
        let element = $(this);
        let url = element.data('url');
        if(confirm('Are you sure to delete the league permanently ?')){
            $.ajax({
                method: "DELETE",
                url: url
            })
            .done(function( msg ) {
                alert( "League deleted: " + msg );
                element.parents('tr').fadeOut(200);
            });
        }
    });
</script>
<script>
    $('body').on('click', '.startLeague', function(){
        let element = $(this);
        let url = element.data('url');
        if(confirm('Are you sure to start the league ?')){
            $.ajax({
                method: "GET",
                url: url
            })
            .done(function( msg ) {
                alert( "League started: " + msg );
                location.reload();
            });
        }
    });
</script>
<script>
    $('body').on('click', '.seeLeague', function(){
        let element = $(this);
        let url = element.data('url');
        window.location.href = url;
    });
</script>
<script>
    $('body').on('change', '.statusCheck', function(){
        let element = $(this);
        element.hide().next('.fa-spinner').show();
        let status = element.prop('checked') ? 1 : 0;
        let url = element.data('url');
        $.ajax({
            method: "PUT",
            url: url,
            data: {status: status}
        })
        .done(function( msg ) {
            alert( "Team updated: " + msg );
            element.show().next('.fa-spinner').hide();
            element.prop('checked', status);
        });
    });
</script>
