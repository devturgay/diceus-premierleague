<div class="row mb-4">
    <div class="col-6">
        <h3>Create a new League!</h3>
    </div>
</div>
<div class="clearfix mb-4"></div>
<form class="row" action="{{ route('leagues.store')}}" method="post">
    @csrf
    <div class="form-group col-12">
        <div class="col-3 ml-0 pl-0">
            <label for="exampleInputEmail1">League Name</label>
            <input name="name" type="text" class="form-control name">
        </div>
    </div>
    <div class="form-group col-md-12 col-sm-12">
        <label for="exampleInputPassword1">Select Teams</label><br>
        <input type="checkbox" class="select-all">
        <label for="exampleInputPassword1">Select All ({{count($teams)}})</label>
        <ul class="teamsToBeSelected" style="list-style-type:none;">
        @foreach ($teams as $team)
            <li>
                <input name="team[]" class="" multiple type="checkbox" value="{{ $team['id'] }}">
                <label>{{ $team['name'] }}</label>
            </li>
        @endforeach
        </ul>
    </div>
    <div class="form-group col-md-3 col-sm-6">
        <button class="btn btn-primary submit" type="button">Submit</button>
    </div>
</form>
<script>
    $('body').on('change','input.select-all',function(){
        $('.teamsToBeSelected').find('input[type="checkbox"]').prop('checked', $(this).prop('checked'));
    });

    $('body').on('click','button.submit',function(){
        var form = $(this).parents('form');
        var teams = handleTeams();
        if(!teams)
            return false;

        var request = {
            name: form.find('input[name="name"]').val(),
            teams: teams
        };

        $.ajax({
            method: "POST",
            url: "{{ route('leagues.store') }}",
            data: request
        })
        .done(function( msg ) {
            alert( "League created: " + msg );
            window.location.href = "{{ route('leagues.index') }}";
        })
        .fail(function ( msg ) {
            alert( "League creation failed: " + msg );
        });
    });

    function handleTeams()
    {
        var teams = [];
        $('input[name="team[]"]:checked').each(function(){
            teams.push($(this).val());
        });
        if(teams.length%2 != 0){
            alert('A league can not be created with odd number of teams -_-');
            return false;
        }
        return teams;
    }
</script>
