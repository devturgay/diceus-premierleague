<h3>Played weeks</h3>
<br>
<div id="accordion">
    @php
        $playedWeeks = array_values(array_unique(array_column($games['playedGames'], 'week_no')));
        $firstWeekNo = $playedWeeks[0];
    @endphp
    @foreach ($playedWeeks as $playedWeek)
        @php $collapsed = $playedWeek == $firstWeekNo ? false : true; @endphp
        <div class="card">
            <div class="card-header" id="heading{{$playedWeek}}">
                <h5 class="mb-0">
                    <button class="btn btn-link {{$collapsed ? 'collapsed' : ''}}" data-toggle="collapse" data-target="#collapse{{$playedWeek}}" aria-expanded="true" aria-controls="collapse{{$playedWeek}}">
                    Week {{$playedWeek}}
                    </button>
                </h5>
            </div>

            <div id="collapse{{$playedWeek}}" class="collapse {{$collapsed ? '' : 'show'}}" aria-labelledby="heading{{$playedWeek}}" data-parent="#accordion">
                <table class="table table-hover">
                    <thead class="thead-primary">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Home Team</th>
                            <th scope="col">Away Team</th>
                            <th scope="col">Home Score</th>
                            <th scope="col">Away Score</th>
                            <th scope="col">Home Point</th>
                            <th scope="col">Away Point</th>
                            {{-- <th scope="col">Actions</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $gameCounter = 1;
                            $thisWeekGames = array_filter($games['playedGames'], function($v) use($playedWeek){
                                return $v['week_no'] == $playedWeek;
                            });
                        @endphp
                        @foreach ($thisWeekGames as $game)
                            @php $w = $game['winner']; @endphp
                            <tr>
                                <td>{{$gameCounter}}</td>
                                <td
                                    class="table-{{$w == 3 ? 'warning' : ($w == 1 ? 'success' : 'danger')}}">
                                    {{$game['home_team_name']}}
                                </td>
                                <td
                                    class="table-{{$w == 3 ? 'warning' : ($w == 2 ? 'success' : 'danger')}}">
                                    {{$game['away_team_name']}}
                                </td>
                                <td>{{$game['home_team_score']}}</td>
                                <td>{{$game['away_team_score']}}</td>
                                <td>{{$game['home_team_point']}}</td>
                                <td>{{$game['away_team_point']}}</td>
                            </tr>
                        @php $gameCounter++; @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endforeach
</div>
