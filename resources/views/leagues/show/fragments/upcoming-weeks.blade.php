<h3>
    Upcoming weeks
    <button
        data-url="{{ route('leagues.playAllWeeks', ['id'=>$league['id']]) }}"
        class="btn btn-danger float-right playAllWeeks">
        <i class="fa fa-play"></i> Play all weeks
    </button>
</h3>
<br>
<div id="accordion2">
    @php
        $upcomingWeeks = array_values(array_unique(array_column($games['upcomingGames'], 'week_no')));
        $firstWeekNo = $upcomingWeeks[0];
    @endphp
    @foreach ($upcomingWeeks as $upcomingWeek)
        @php $collapsed = $upcomingWeek == $firstWeekNo ? false : true; @endphp
        <div class="card">
            <div class="card-header" id="heading{{$upcomingWeek}}">
                <h5 class="mb-0">
                    <button class="btn btn-link {{$collapsed ? 'collapsed' : ''}}" data-toggle="collapse" data-target="#collapse{{$upcomingWeek}}" aria-expanded="true" aria-controls="collapse{{$upcomingWeek}}">
                    Week {{$upcomingWeek}}
                    </button>
                    @if (!$collapsed)
                        <button data-url="{{route('leagues.playweek', ['id'=>$league['id']])}}" class="playNextWeek btn btn-primary float-right"><i class="fa fa-play"></i> Play Week</button>
                    @endif
                </h5>
            </div>

            <div id="collapse{{$upcomingWeek}}" class="collapse {{$collapsed ? '' : 'show'}}" aria-labelledby="heading{{$upcomingWeek}}" data-parent="#accordion2">
                <table class="table table-hover">
                    <thead class="thead-primary">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Home Team</th>
                            <th scope="col">Away Team</th>
                            <th scope="col">Home Score</th>
                            <th scope="col">Away Score</th>
                            <th scope="col">Home Point</th>
                            <th scope="col">Away Point</th>
                            {{-- <th scope="col">Actions</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $gameCounter = 1;
                            $thisWeekGames = array_filter($games['upcomingGames'], function($v) use($upcomingWeek){
                                return $v['week_no'] == $upcomingWeek;
                            });
                        @endphp
                        @foreach ($thisWeekGames as $game)
                            <tr>
                                <td>{{$gameCounter}}</td>
                                <td>{{$game['home_team_name']}}</td>
                                <td>{{$game['away_team_name']}}</td>
                                <td>{{$game['home_team_score']}}</td>
                                <td>{{$game['away_team_score']}}</td>
                                <td>{{$game['home_team_point']}}</td>
                                <td>{{$game['away_team_point']}}</td>
                            </tr>
                        @php $gameCounter++; @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endforeach
</div>
<script>
    $('body').on('click','.playNextWeek',function(){
        let url = $(this).data('url');
        if(confirm('Are you sure to play this week ?')){
            $.ajax({
                method: "GET",
                url: url
            })
            .done(function( msg ) {
                alert( "Week played: " + msg );
                location.reload();
            });
        }
    });
</script>
<script>
    $('body').on('click','.playAllWeeks',function(){
        let url = $(this).data('url');
        if(confirm('Are you sure to play all weeks at once ?')){
            $.ajax({
                method: "GET",
                url: url
            })
            .done(function( msg ) {
                alert( "League finished: " + msg );
                location.reload();
            });
        }
    });
</script>
