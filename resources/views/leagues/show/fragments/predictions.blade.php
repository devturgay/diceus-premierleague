<div class="row mb-4 mt-2">
    <div class="col-sm-12">
        <h3>League Predictions</h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-hover">
            <thead class="thead-primary">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Team Name</th>
                    <th scope="col">Total point</th>
                    <th scope="col">Championship Prediction</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $counter = 1;
                @endphp
                @foreach ($table as $team)
                    @php
                        $homeData = explode('-', $team['in_home']);
                        $awayData = explode('-', $team['in_away']);
                        $totalPoint = $homeData[2]+$awayData[2];

                        $prediction = number_format(100*($totalPoint/$sumOfTotalPoints),2,'.','');
                    @endphp
                    <tr>
                        <td>{{$counter}}</td>
                        <td>{{$team['name']}}</td>
                        <td>{{$totalPoint}}</td>
                        <td>{{$prediction}}%</td>
                    </tr>
                @php $counter++; @endphp
                @endforeach
            </tbody>
        </table>
    </div>
</div>
