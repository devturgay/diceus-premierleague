<div class="row mb-4 mt-2">
    <div class="col-sm-12">
        <h3>League Table</h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-hover">
            <thead class="thead-primary">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Team Name</th>
                    <th scope="col">Scored goals</th>
                    <th scope="col">Missed goals</th>
                    <th scope="col">Total point</th>
                    <th scope="col">Won</th>
                    <th scope="col">Lost</th>
                    <th scope="col">Draw</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $counter = 1;
                    $sumOfTotalPoints = 0;
                @endphp
                @foreach ($table as $team)
                    @php
                        $homeData = explode('-', $team['in_home']);
                        $awayData = explode('-', $team['in_away']);
                        $totalPoint = $homeData[2]+$awayData[2];
                        $sumOfTotalPoints += $totalPoint;
                    @endphp
                    <tr>
                        <td>{{$counter}}</td>
                        <td>{{$team['name']}}</td>
                        <td>{{$homeData[0]}}</td>
                        <td>{{$homeData[1]}}</td>
                        <td>{{$totalPoint}}</td>
                        <td>{{$homeData[3]+$awayData[3]}}</td>
                        <td>{{$homeData[4]+$awayData[4]}}</td>
                        <td>{{$homeData[5]+$awayData[5]}}</td>
                    </tr>
                @php $counter++; @endphp
                @endforeach
            </tbody>
        </table>
    </div>
</div>
