<div class="row mb-4">
    <div class="col-6">
        <h3>Edit the Team -> {{ $team['name'] }}</h3>
    </div>
</div>
<div class="clearfix mb-4"></div>
<form class="row" action="/teams" method="post">
    @csrf
    <div class="form-group col-12">
        <div class="col-3 ml-0 pl-0">
            <label for="exampleInputEmail1">Team Name</label>
            <input name="name" type="text" class="form-control name" value="{{ $team['name'] }}">
        </div>
    </div>
    <div class="form-group col-md-3 col-sm-6">
        <label for="exampleInputPassword1">Home power min%</label>
        <input name="home_power_min" type="number" class="form-control power pw_min" id="exampleInputPassword1" value="{{ $team['home_power_min'] }}">
    </div>
    <div class="form-group col-md-3 col-sm-6">
        <label for="exampleInputPassword1">Home power max%</label>
        <input name="home_power_max" type="number" class="form-control power pw_max" id="exampleInputPassword1" value="{{ $team['home_power_max'] }}">
    </div>
    <div class="form-group col-md-3 col-sm-6">
        <label for="exampleInputPassword1">Away power min%</label>
        <input name="away_power_min" type="number" class="form-control power pw_min" id="exampleInputPassword1" value="{{ $team['away_power_min'] }}">
    </div>
    <div class="form-group col-md-3 col-sm-6">
        <label for="exampleInputPassword1">Away power max%</label>
        <input name="away_power_max" type="number" class="form-control power pw_max" id="exampleInputPassword1" value="{{ $team['away_power_max'] }}">
    </div>
    <div class="form-group col-md-3 col-sm-6">
        <button class="btn btn-primary submit" type="button">Submit</button>
    </div>
</form>
<script>
    $(document).ready(function(){
        checkAllMaxValuesGreater();
    });
    function checkAllMaxValuesGreater()
    {
        var hideSubmit = false;
        $('input.pw_max').each(function(){
            let maxVal = parseInt($(this).val());
            let minVal = $(this).parent('div').prev('div').find('input.pw_min').val();
            $(this).css('border-color','#ced4da').parent('div').find('.errorLabel').remove();

            if(maxVal < minVal){
                hideSubmit = true;
                var errorLabel = `<small id="emailHelp" class="form-text text-muted errorLabel">Maximum value should always be greater or equal than minimum value</small>`;
                $(this).css('border-color','red').parent('div').append(errorLabel);
            }
        });
        $('button.submit').show();
        if(hideSubmit)
            $('button.submit').hide();
    }

    $('body').on('keyup','.power',function(){
        checkAllMaxValuesGreater();
    });

    $('body').on('click','button.submit',function(){
        var form = $(this).parents('form');
        var request = {
            name: form.find('input[name="name"]').val(),
            home_power_min: form.find('input[name="home_power_min"]').val(),
            home_power_max: form.find('input[name="home_power_max"]').val(),
            away_power_min: form.find('input[name="away_power_min"]').val(),
            away_power_max: form.find('input[name="away_power_max"]').val(),
        };

        $.ajax({
            method: "PUT",
            url: "{{ route('teams.update', ['team'=>$team['id']]) }}",
            data: request
        })
        .done(function( msg ) {
            alert( "Data Saved: " + msg );
            window.location.href = "{{ route('teams.index') }}";
        });
    });
</script>
