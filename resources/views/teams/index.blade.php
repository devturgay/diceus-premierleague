<div class="row mb-4">
    <div class="col-12">
        <h3 class="float-left">Existing Teams</h3>
        <button class="btn btn-primary float-right" onclick="window.location.href='{{route('teams.create')}}'"><i class="fa fa-plus"></i> Add team</button>
    </div>
</div>

<table class="table table-hover">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Team Name</th>
            <th scope="col">Status</th>
            <th scope="col">Min power - home games</th>
            <th scope="col">Max power - home games</th>
            <th scope="col">Min power - away games</th>
            <th scope="col">Max power - away games</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @php ($counter = 1)
        @foreach ($teams as $team)
        <tr>
            <th scope="row">{{ $counter }}</th>
            <td>{{ $team['name'] }}</td>
            <td>
                <input
                    data-url="{{ route('teams.status', ['team'=>$team['id']]) }}"
                    class="statusCheck"
                    type="checkbox"
                    {{ $team['status'] == 1 ? 'checked' : '' }}
                >
                <i class="fa fa-spinner fa-spin" style="display:none;"></i>
            </td>
            <td>{{ $team['home_power_min'] }}</td>
            <td>{{ $team['home_power_max'] }}</td>
            <td>{{ $team['away_power_min'] }}</td>
            <td>{{ $team['away_power_max'] }}</td>
            <td>
                <button class="btn btn-danger deleteTeam" data-url="{{ route('teams.destroy', ['team'=>$team['id']]) }}">
                    <i class="fa fa-trash"></i>
                </button>
                <button class="btn btn-success" onclick="window.location.href='{{ route('teams.edit', ['team'=>$team['id']]) }}'">
                    <i class="fa fa-edit"></i>
                </button>
            </td>
        </tr>
        @php ($counter++)
        @endforeach
    </tbody>
</table>
<script>
    $('body').on('click', '.deleteTeam', function(){
        let element = $(this);
        let url = element.data('url');
        if(confirm('Are you sure to delete the team permanently ?')){
            $.ajax({
                method: "DELETE",
                url: url
            })
            .done(function( msg ) {
                alert( "Team deleted: " + msg );
                element.parents('tr').fadeOut(200);
            });
        }
    });
</script>
<script>
    $('body').on('change', '.statusCheck', function(){
        let element = $(this);
        element.hide().next('.fa-spinner').show();
        let status = element.prop('checked') ? 1 : 0;
        let url = element.data('url');
        $.ajax({
            method: "PUT",
            url: url,
            data: {status: status}
        })
        .done(function( msg ) {
            alert( "Team updated: " + msg );
            element.show().next('.fa-spinner').hide();
            element.prop('checked', status);
        });
    });
</script>
